#!/bin/bash

# Constants
RAW_NAME='raw.png'
IMAGE_DIR="$(pwd)/a-images"
ANSWER_DIR="$(pwd)/a-text"
RESULT_DIR="$(pwd)/results"

# Clean out working directory
./clear.sh

#Confirm existence of directories
if [ ! -d $IMAGE_DIR ]; then
	mkdir -p $IMAGE_DIR
fi

if [ ! -d $ANSWER_DIR ]; then
	mkdir -p $ANSWER_DIR
fi

if [ ! -d $RESULT_DIR ]; then
	mkdir -p $RESULT_DIR
fi

# Set stuff up for current show
case $1 in
	"hq")
		echo "Game: HQ"
		QUESTION_CROP="980x490+50+340"
		A1_CROP="830x110+120+860"
		A2_CROP="830x110+120+1060"
		A3_CROP="830x110+120+1260"
		;;
	"cs")
		echo "Game: Cash Show"
		QUESTION_CROP="980x290+50+360"
		A1_CROP="850x120+115+800"
		A2_CROP="850x120+115+1000"
		A3_CROP="850x120+115+1200"
	;;
	"btq")
		echo "Game: Beat the Q"
		QUESTION_CROP="950x375+65+300"
		A1_CROP="815x115+130+705"
		A2_CROP="815x115+130+895"
		A3_CROP="815x115+130+1085"
	;;
	"swag")
		echo "Game: SwagIQ"
		QUESTION_CROP="1080x345+0+480"
		A1_CROP="815x115+145+845"
		A2_CROP="815x115+145+1000"
		A3_CROP="815x115+145+1165"
	;;
esac


# Generate images
if [ -z $2 ]; then
	adb exec-out screencap -p > $RAW_NAME               # Raw screenshot
else
	echo "Running test with $2"
	RAW_NAME=$2
fi
convert $RAW_NAME -crop $QUESTION_CROP question.png  # Question
convert $RAW_NAME -crop $A1_CROP $IMAGE_DIR/a1.png   # First answer
convert $RAW_NAME -crop $A2_CROP $IMAGE_DIR/a2.png   # Second answer
convert $RAW_NAME -crop $A3_CROP $IMAGE_DIR/a3.png   # Third answer

function ocr {
	 tesseract $1 $2  &> /dev/null
	if [ ! -z ${3+x} ]; then
		cat $2.txt | head -n1 >> $3
	else
		cat $2.txt | xargs >> $1.txt
	fi
}

# Generate question text
ocr "question.png" "question-raw" 

# Generate answer texts
for f in $IMAGE_DIR/*; do
	filename=$(basename $f | cut -f 1 -d '.')
	ocr $f $ANSWER_DIR/$filename-tmp $ANSWER_DIR/$filename.txt
done

echo

# Prep search
question=$(cat question.png.txt)
if [ $1 == "btq" ]; then
	question=${question:2}
elif [ $1 == "swag" ]; then
	question=${question//'*'/}
fi

echo $question

tmp_question_url=${question// /+}
question_url=${tmp_question_url//'?'/%3F}

# Searches for total results when searching for question and answer
function search1 {
	ans=$(cat $1)
	a="\"${ans// /+}\""
	fname=$(basename $1)
	num=${fname:1:1}
	url="http://www.google.com/search?q=$question_url+\"$a\"&oq=$question_url+\"$a\""
	curl -sA "Chrome" -L $url -o $RESULT_DIR/$num-tot_res.txt 
}

# Counts occurrences of each answer on the first page of results
function search2 {
	url="http://www.google.com/search?q=$question_url&oq=$question_url"
	curl -sA "Chrome" -L $url -o $RESULT_DIR/gen-search.txt
	for f in $ANSWER_DIR/*; do
		ans=" $(cat $f) "
		fname=$(basename $f)
		num=${fname:1:1}
		if [ ${fname:(-5)} != "p.txt" ]; then
			cat $RESULT_DIR/gen-search.txt | grep -o "$ans" -i | wc -l >> $RESULT_DIR/$num-tot_occ.txt
		fi
	done
}

i=1

for f in $ANSWER_DIR/*; do
	fname=$(basename $f)
	if [ ${fname:(-5)} != "p.txt" ]; then
		echo "$i. $(cat $f)"
		i=$(($i+1))
		search1 $f &
	fi
done

search2 &

wait

echo

# Parse search results
function parseNum1 {
	res_num=$(awk '{split($0,a,"id=\"resultStats\">About "); split(a[2],b," results"); print b[1]}' $1)
	if [ -z $res_num ]; then 
		res_num=$(awk '{split($0,a,"id=\"resultStats\">"); split(a[2],b," results"); print b[1]}' $1)
	fi
	num_re='^[0-9]+$'
	if ! [[ ${res_num//','/} =~ $num_re ]]; then
		res_num=0
	fi
	fname=$(basename $f)
	num=${fname:0:1}
	echo "            $res_num total results"
}

function parseNum2 {
	fname=$(basename $1)
	num=${fname:0:1}
	echo "Answer $num: $(cat $1) results on first page"
}


for f in $RESULT_DIR/*; do
	fname=$(basename $f)
	ext=$(basename $f | cut -f 2 -d '_' | cut -f 1 -d '.')
	num=${fname:1:1}
	if [ $ext == "res" ]; then
		parseNum1 $f
		res[$num]=$res_num
	elif [ $ext == "occ" ]; then
		parseNum2 $f
		occ[$num]=$(cat $f)
	fi
done

echo

r1=$(echo ${res[1]}); res1=${r1//,/}
r2=$(echo ${res[2]}); res2=${r2//,/}
r3=$(echo ${res[3]}); res3=${r3//,/}
o1=$(echo ${occ[1]}); occ1=${o1//,/}
o2=$(echo ${occ[2]}); occ2=${o2//,/}
o3=$(echo ${occ[3]}); occ3=${o3//,/}

while [ ${#res1} -lt ${#res2} ]; do
	res1="0$res1"
done
while [ ${#res1} -lt ${#res3} ]; do
	res1="0$res1"
done
while [ ${#res2} -lt ${#res1} ]; do
	res2="0$res2"
done
while [ ${#res2} -lt ${#res3} ]; do
	res2="0$res2"
done
while [ ${#res3} -lt ${#res1} ]; do
	res3="0$res3"
done
while [ ${#res3} -lt ${#res2} ]; do
	res3="0$res3"
done

while [ ${#occ1} -lt ${#occ2} ]; do
	occ1="0$occ1"
done
while [ ${#occ1} -lt ${#occ3} ]; do
	occ1="0$occ1"
done
while [ ${#occ2} -lt ${#occ1} ]; do
	occ2="0$occ2"
done
while [ ${#occ2} -lt ${#occ3} ]; do
	occ2="0$occ2"
done
while [ ${#occ3} -lt ${#occ1} ]; do
	occ3="0$occ3"
done
while [ ${#occ3} -lt ${#occ2} ]; do
	occ3="0$occ3"
done

vals[1]="$occ1$res1 1"
vals[2]="$occ2$res2 2"
vals[3]="$occ3$res3 3"

IFS=$'\n' sorted=($(sort -r <<<"${vals[*]}"))
unset IFS

out=""

for i in 0 1; do
	out="$out$(echo ${sorted[$i]} | cut -f 2 -d ' ')"

	current=$(echo ${sorted[$i]} | cut -f 1 -d ' ')
	next=$(echo ${sorted[$i+1]} | cut -f 1 -d ' ')

	if [ $current -eq $next ]; then
		out="$out="
	else
		out="$out "
	fi
done

out="$out$(echo ${sorted[2]} | cut -f 2 -d ' ')"

echo $out
